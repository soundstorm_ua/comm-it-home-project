import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';

const moduleImports = [
  ConfigModule.forRoot({ isGlobal: true }),
  MongooseModule.forRoot(process.env.DATABASE_URI, {
    dbName: process.env.DATABASE_NAME,
    auth: {
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASS,
    },
  }),

  // feature module
  UserModule,
];

if (process.env.NODE_ENV === 'production') {
  moduleImports.unshift(
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../..', 'client', 'dist'),
    }),
  );
}

@Module({
  imports: moduleImports,
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
