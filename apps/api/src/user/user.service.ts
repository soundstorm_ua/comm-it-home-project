import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { Model } from 'mongoose';

import { CreateUserInput, UpdateUserInput } from './model/user.input';
import { UserPayload } from './model/user.payload';
import { User } from './user.schema';

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}

  async createUser({ password, ...restBody }: CreateUserInput) {
    const salt = await bcrypt.genSalt();
    const passwordHash = await bcrypt.hash(password, salt);
    const createdUser = new this.userModel({
      ...restBody,
      password: passwordHash,
    });

    return await createdUser.save();
  }

  async findUser(id: string) {
    const user = await this.userModel.findOne({ _id: id }).exec();

    if (!user) {
      throw new NotFoundException(`User with _id:${id} not found `);
    }

    return user;
  }

  async listUser(): Promise<UserPayload[]> {
    const users = await this.userModel.find();
    return users;
  }

  async updateUser(id: string, body: UpdateUserInput): Promise<UserPayload> {
    await this.userModel.updateOne({ _id: id }, body);
    const updatedUser = this.userModel.findById(id);
    return updatedUser;
  }

  async deleteUser(id: string): Promise<void> {
    await this.userModel.deleteOne({ _id: id });
  }
}
