import { OmitType } from '@nestjs/swagger';
import { IsPhoneNumber, IsString, MaxLength, MinLength } from 'class-validator';

export class CreateUserInput {
  @IsString()
  @MaxLength(32)
  userName: string;

  @IsPhoneNumber()
  phoneNumber: string;

  @IsString()
  @MinLength(6)
  @MaxLength(12)
  password: string;
}

export class UpdateUserInput extends OmitType(CreateUserInput, ['password'] as const) {}
