import { useCallback } from "react";
import { ObjectSchema } from "yup";

type AnyObject = Record<string, unknown>;

const getSchemaValues = (schema: ObjectSchema<AnyObject>, values?: AnyObject): AnyObject => {
  try {
    return schema.cast(values ?? {}, {
      assert: false,
      stripUnknown: true,
      context: values ?? {},
    });
  } catch (error) {
    console.debug(error);

    return {};
  }
};

export const useSchemaValues = <T extends AnyObject>(schema: ObjectSchema<T>) => {
  const deepMapKeys = useCallback((obj: unknown): unknown => {
    if (Array.isArray(obj)) {
      return obj.map(deepMapKeys);
    }

    if (typeof obj === 'object') {
      if (obj instanceof Date) {
        return new Date(obj);
      }

      return obj ? Object.entries(obj).reduce((acc: AnyObject, [key, val]) => {
        if (val !== null && typeof val === 'object') {
          acc[key] = deepMapKeys(val as AnyObject);
        } else {
          acc[key] = val;
        }

        return acc;
      }, {}) : null;
    }

    return obj;
  }, []);

  const getIntlSchemaValues = useCallback(
    (values?: T): T => deepMapKeys(getSchemaValues(schema, values)) as T,
    [deepMapKeys, schema],
  );

  return getIntlSchemaValues;
};
