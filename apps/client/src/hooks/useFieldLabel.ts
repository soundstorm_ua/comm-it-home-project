import { ReactNode } from "react";
import { useFormContext } from "react-hook-form";
import { ObjectSchema, reach, SchemaFieldDescription, SchemaRefDescription } from "yup";

const isWithoutLabel = (field: SchemaFieldDescription): field is SchemaRefDescription => {
  return !Object.keys(field).includes('label');
};

type AnyObject = Record<string, unknown>;

export const useFieldLabel = <T extends AnyObject>(
  schema: ObjectSchema<T>,
  name: string,
  label?: string,
): ReactNode | string => {
  const { getValues } = useFormContext();
  let resultLabel = label;

  if (typeof label === 'undefined') {
    const values = getValues();
    const field = reach(schema, name, values, values)
      .resolve({
        value: values,
        parent: values,
        context: values,
      })
      .describe();

    if (!isWithoutLabel(field)) {
      resultLabel = field.label;
    }
  }

  return resultLabel ?? '';
};