import { isValidPhoneNumber } from 'libphonenumber-js';
import { addMethod, string, StringSchema as YupStringSchema } from 'yup';

declare module 'yup' {
  interface StringSchema {
    password(): YupStringSchema;
    hasUpperCase(number: number): YupStringSchema;
    hasLowerCase(number: number): YupStringSchema;
    hasSpecialChar(number: number): YupStringSchema;
    phone(): YupStringSchema;
  }
}

addMethod<YupStringSchema>(string, 'hasUpperCase', function (number: number) {
  return this.test({
    name: 'hasUpperCase',
    message: 'Shall have uppercase char',
    params: {
      number,
    },
    test: (value = '') => /[A-Z]/.test(value),
  });
});

addMethod<YupStringSchema>(string, 'hasLowerCase', function (number: number) {
  return this.test({
    name: 'hasLowerCase',
    message: 'Shall have lowercase char',
    params: {
      number,
    },
    test: (value = '') => /[a-z]/.test(value),
  });
});

addMethod<YupStringSchema>(string, 'hasSpecialChar', function (number: number) {
  return this.test({
    name: 'hasSpecialChar',
    message: 'Shall have special char',
    params: {
      number,
    },
    test: (value = '') => /[\^_=!#$%&()*+\-.:'/?@ ]/.test(value),
  });
});

addMethod<YupStringSchema>(string, 'password', () =>
  string()
    .min(6, 'Shall be at least 6 chars')
    .max(12, 'Shall be 12 chars maximum')
    .hasSpecialChar(1)
    .hasUpperCase(1)
    .hasLowerCase(1)
    .transform((value?: string) => value ?? ''),
);

addMethod<YupStringSchema>(
  string,
  'phone',
  function (message?: string) {
    return this.test({
      name: 'phone',
      message: message ?? 'Shall be a valid phone number',
      test: (value = '') => !value || isValidPhoneNumber(value),
    });
  },
);