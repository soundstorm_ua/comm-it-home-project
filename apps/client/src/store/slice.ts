import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { AddUserModel } from "../containers/AddUser/AddUser.schema";

export type User = Omit<AddUserModel, "password" | "confirmPassword"> & {
  _id: string;
};

export interface Store {
  currentUser: User | null;
}

const initialState: Store = {
  currentUser: null,
};

export const userSlice = createSlice({
  name: "users",
  initialState,
  reducers: {
    saveUser: (state, action: PayloadAction<User>) => {
      state.currentUser = action.payload;
    },
  },
});

export const { saveUser } = userSlice.actions;

export default userSlice.reducer;
