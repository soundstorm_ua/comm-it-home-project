import "./index.css";
import "./extendYup.ts";

import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";

import App from "./App.tsx";
import { store } from "./store/store.ts";

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const rootElement: HTMLElement = document.getElementById("root")!;

ReactDOM.createRoot(rootElement).render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);
