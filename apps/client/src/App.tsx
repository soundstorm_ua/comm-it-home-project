import "./App.css";

import Container from "@mui/material/Container";
import CssBaseline from "@mui/material/CssBaseline";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { useState } from "react";

import { MainTabs } from "./containers/Main";

function App() {
  const [queryClient] = useState(() => new QueryClient());

  return (
    <>
      <CssBaseline />
      <Container maxWidth="lg">
        <QueryClientProvider client={queryClient}>
          <MainTabs />
        </QueryClientProvider>
      </Container>
    </>
  );
}

export default App;
