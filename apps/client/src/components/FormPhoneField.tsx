import { MuiTelInput, MuiTelInputProps } from "mui-tel-input";
import { Controller, useFormContext } from "react-hook-form";
import { ObjectSchema } from "yup";

import { useFieldLabel } from "../hooks/useFieldLabel";

type FormPhoneFieldProps<T extends Record<string, unknown>> =
  MuiTelInputProps & {
    name: string;
    schema: ObjectSchema<T>;
  };

export function FormPhoneField<T extends Record<string, unknown>>({
  name,
  schema,
  ...rest
}: FormPhoneFieldProps<T>) {
  const { control } = useFormContext();
  const label = useFieldLabel(schema, name);

  return (
    <Controller
      name={name}
      control={control}
      render={({
        field: { ref: fieldRef, value, ...fieldProps },
        fieldState,
      }) => (
        <MuiTelInput
          {...fieldProps}
          {...rest}
          // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
          value={value ?? ""}
          inputRef={fieldRef}
          label={label}
          error={fieldState.invalid}
          variant="outlined"
          fullWidth
          margin="dense"
        />
      )}
    />
  );
}
