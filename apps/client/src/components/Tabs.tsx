import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import { ComponentType, ReactNode, SyntheticEvent, useState } from "react";

interface TabPanelProps {
  children?: ReactNode;
  index: number;
  value: number;
}

function CustomTabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div role="tabpanel" hidden={value !== index} {...other}>
      {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
    </div>
  );
}

export interface TabProps {
  Component: ComponentType | null;
  label: string;
  key: string;
}

export function TabsComponent({ tabs }: { tabs: TabProps[] }) {
  const [value, setValue] = useState(0);

  const handleChange = (_event: SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };
  return (
    <Box sx={{ width: "100%" }}>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          {tabs.map(({ key, label }) => (
            <Tab
              key={`tab_${key}`}
              label={label}
              sx={{
                "&:focus": { outline: 0 },
              }}
            />
          ))}
        </Tabs>
      </Box>
      {tabs.map(({ key, Component }, index) => (
        <CustomTabPanel key={`tabPanel_${key}`} value={value} index={index}>
          {Component ? <Component /> : null}
        </CustomTabPanel>
      ))}
    </Box>
  );
}
