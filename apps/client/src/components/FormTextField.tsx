import TextField, { TextFieldProps } from "@mui/material/TextField";
import { Controller, useFormContext } from "react-hook-form";
import { ObjectSchema } from "yup";

import { useFieldLabel } from "../hooks/useFieldLabel";

type FormTextFieldProps<T extends Record<string, unknown>> = TextFieldProps & {
  name: string;
  schema: ObjectSchema<T>;
};

export function FormTextField<T extends Record<string, unknown>>({
  name,
  schema,
  ...rest
}: FormTextFieldProps<T>) {
  const { control } = useFormContext();
  const label = useFieldLabel(schema, name);

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <TextField
          {...field}
          // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
          value={field.value ?? ""}
          {...rest}
          label={label}
          variant="outlined"
          fullWidth
          margin="dense"
        />
      )}
    />
  );
}
