import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { useQuery } from "@tanstack/react-query";
import { useEffect } from "react";

import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { saveUser, User } from "../../store/slice";
import { RootState } from "../../store/store";

export function PreviewUserTab() {
  const dispatch = useAppDispatch();
  const user = useAppSelector((state: RootState) => state.users.currentUser);

  const { isLoading, error, data } = useQuery({
    queryKey: ["user", user?._id],
    enabled: !!user?._id,
    queryFn: async () => {
      const response = await fetch(`/api/users/${user?._id}`, {
        method: "GET",
        headers: { "content-type": "application/json" },
      });

      if (response.ok) {
        return (await response.json()) as User;
      }

      return;
    },
  });

  useEffect(() => {
    if (!data) {
      return;
    }

    const { _id, userName, phoneNumber } = data;
    dispatch(saveUser({ _id, userName, phoneNumber }));
  }, [data, dispatch]);

  if (isLoading) return "Loading...";

  if (error) return "An error has occurred: " + error.message;

  const userCard = (
    <>
      <Stack direction="row" spacing={1}>
        <Typography variant="subtitle1" fontWeight="bold">
          User name:
        </Typography>
        <Typography variant="subtitle1">{user?.userName}</Typography>
      </Stack>
      <Stack direction="row" spacing={1}>
        <Typography variant="subtitle1" fontWeight="bold">
          Phone number:
        </Typography>
        <Typography variant="subtitle1">{user?.phoneNumber}</Typography>
      </Stack>
    </>
  );

  return (
    <Card sx={{ minWidth: 275 }}>
      <CardContent>
        {user?._id ? (
          userCard
        ) : (
          <Typography variant="subtitle1">No user yet</Typography>
        )}
      </CardContent>
    </Card>
  );
}
