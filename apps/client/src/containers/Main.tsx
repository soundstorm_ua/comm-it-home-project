import { TabProps, TabsComponent } from "../components/Tabs";
import { AddUserTab } from "./AddUser/AddUser.tab";
import { PreviewUserTab } from "./PreviewUser/PreviewUser.tab";

const tabs: TabProps[] = [
  {
    key: "addUser",
    label: "Add user",
    Component: AddUserTab,
  },
  {
    key: "previewUser",
    label: "Preview user",
    Component: PreviewUserTab,
  },
];

export function MainTabs() {
  return <TabsComponent tabs={tabs} />;
}
