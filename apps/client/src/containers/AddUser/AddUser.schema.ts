import { InferType, object, ref, string } from "yup";

export const addUserValidationSchema = object().shape({
  userName: string().max(32).default('').label('User name').required(),
  password: string().password().default('').label('Password').required(),
  confirmPassword: string()
    .required()
    .label('Confirm password')
    .default('')
    .oneOf([ref('password')], 'validation.password-not-match'),
  phoneNumber: string().phone().label('Phone number').nullable().default(null),
});

export type AddUserModel = InferType<typeof addUserValidationSchema>;
