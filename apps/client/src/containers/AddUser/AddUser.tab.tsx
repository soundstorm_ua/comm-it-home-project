import { yupResolver } from "@hookform/resolvers/yup";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { FormProvider, SubmitHandler, useForm } from "react-hook-form";

import { FormPhoneField } from "../../components/FormPhoneField";
import { FormTextField } from "../../components/FormTextField";
import { useSchemaValues } from "../../hooks/useSchemaValues";
import { useAppDispatch } from "../../store/hooks";
import { saveUser, User } from "../../store/slice";
import { AddUserModel, addUserValidationSchema } from "./AddUser.schema";

const Form = styled("form")(() => ({
  width: "100%",
  maxWidth: "300px",
  textAlign: "start",
}));

export function AddUserTab() {
  const dispatch = useAppDispatch();
  const queryClient = useQueryClient();

  const getSchemaValues = useSchemaValues<AddUserModel>(
    addUserValidationSchema
  );

  const methods = useForm<AddUserModel>({
    resolver: async (data, context, options) => {
      const resolverResult = await yupResolver(addUserValidationSchema)(
        data,
        {
          ...data,
          ...context,
        },
        options
      );

      console.debug("formData", data);
      console.debug("validation result", resolverResult);

      return resolverResult;
    },
    defaultValues: getSchemaValues({
      userName: "test",
      phoneNumber: "+380500000000",
      password: "Qwerty123$",
      confirmPassword: "Qwerty123$",
    }),
    mode: "onChange",
  });

  const mutation = useMutation({
    mutationFn: async (data: AddUserModel) => {
      const response = await fetch("/api/users", {
        method: "POST",
        headers: { "content-type": "application/json" },
        body: JSON.stringify(data),
      });

      if (response.ok) {
        return (await response.json()) as User;
      }

      return null;
    },
    onSuccess: async (data: User | null) => {
      if (!data) {
        return;
      }
      const { _id, userName, phoneNumber } = data;
      dispatch(saveUser({ _id, userName, phoneNumber }));

      try {
        await queryClient.invalidateQueries({ queryKey: ["user"] });
      } catch (error) {
        console.error(error);
      }
    },
  });

  const onSubmit: SubmitHandler<AddUserModel> = (data: AddUserModel) => {
    mutation.mutate(data);
  };

  return (
    <FormProvider {...methods}>
      {/* eslint-disable-next-line @typescript-eslint/no-misused-promises */}
      <Form onSubmit={methods.handleSubmit(onSubmit)}>
        <FormTextField name="userName" schema={addUserValidationSchema} />
        <FormPhoneField name="phoneNumber" schema={addUserValidationSchema} />
        <FormTextField
          name="password"
          schema={addUserValidationSchema}
          type="password"
        />
        <FormTextField
          name="confirmPassword"
          schema={addUserValidationSchema}
          type="password"
        />
        <Button
          type="submit"
          variant="contained"
          size="large"
          disabled={
            !!Object.keys(methods.formState.errors).length || mutation.isPending
          }
          sx={{ mt: 2 }}
        >
          Submit
        </Button>
      </Form>
    </FormProvider>
  );
}
