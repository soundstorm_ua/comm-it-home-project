# Base image
FROM node:latest AS base
WORKDIR /app

# Alpine image
FROM node:21.7-alpine AS alpine
WORKDIR /app

# Prune project
FROM base AS pruner
RUN npm install turbo --global
COPY . .
RUN turbo prune --scope=api --scope=client --docker

# Build the projects in dev
FROM base AS dev-deps
COPY --from=pruner /app/out/json/ .
COPY --from=pruner /app/out/package-lock.json ./package-lock.json
RUN npm ci

# Build the projects in prod
FROM alpine AS prod-deps
COPY --from=pruner /app/out/json/ .
COPY --from=pruner /app/out/package-lock.json ./package-lock.json
RUN npm ci --production

RUN ( wget -q -O /dev/stdout https://gobinaries.com/tj/node-prune | sh ) \
  && node-prune

# Build the projects
FROM base AS builder
COPY --from=dev-deps /app/ .
COPY --from=pruner /app/out/full/ .
COPY turbo.json turbo.json
RUN npm install turbo --global
RUN turbo run build

# Final image
FROM alpine AS runner
RUN addgroup --system --gid 1001 nestjs
RUN adduser --system --uid 1001 nestjs
USER nestjs

COPY --from=prod-deps --chown=nestjs:nestjs /app/node_modules ./node_modules
COPY --from=prod-deps --chown=nestjs:nestjs /app/*.json ./
COPY --from=builder --chown=nestjs:nestjs /app/apps/api/dist ./apps/api/dist/
COPY --from=builder --chown=nestjs:nestjs /app/apps/client/dist ./apps/client/dist/

ENV NODE_ENV=production
EXPOSE ${API_PORT}

ENV DATABASE_URI=${DATABASE_URI}

WORKDIR /app/apps/api/dist

CMD node main.js