# comm-it-home-project



## Getting started

1. use node >= 21.7
2. install latest docker
3. install node modules - npm i

I've added `.env` and `.env.dev` to repository as they are only for test project.

## To run dev env

```
npm run dev
```

## In docker

```
npm run docker:dev
```

## To run prod env

```
npm run build
npm run start
```

## To run prod env in docker

```
npm run docker:start
```
